# Graph Card component

- TypeScript (Optional)

- Figma : https://www.figma.com/file/XVlJQna1AI67PNrfCGj6tL/GraphCard?type=design&node-id=0%3A1&mode=design&t=Y1S5epNFH4GwVPDD-1

- Figma Prototype : https://www.figma.com/proto/XVlJQna1AI67PNrfCGj6tL/GraphCard?page-id=0%3A1&type=design&node-id=1-141&viewport=-103%2C374%2C0.41&t=FEbHuiR8BtGLUOSF-1&scaling=min-zoom&mode=design

**Component's behavior**

![alt Graph Card gif](graph-card.gif)

## Authorized libraries

> React

> ChartJs (GraphCardBodyDonut) : https://www.chartjs.org/docs/latest/samples/other-charts/doughnut.html

## Component

**We ask that you adhere to this breakdown**

1. GraphCard
2. GraphCardHeader
3. GraphCardBody
4. GraphCardBodyBar
5. GraphCardBodyText
6. GraphCardBodyDonut

**Like this**

![alt Components](components.png)

## Json

### 4 - GraphCardBodyBar

**Here are the data to be used.**

in object.js

`let GraphCardBodyBar`

### 5 - GraphCardBodyText

in object.js

`let GraphCardBodyText`

### 6 - GraphCardBodyDonut

in object.js

`let GraphCardBodyDonut`

## Font 

Inter

`Inter.zip`