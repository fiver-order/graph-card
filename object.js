let GraphCardBodyBar = 
{
	methodologyLabel: "Lorem Ipsum",
	documentLabel: "Lorem ipsum dolor sit amet",
	documentId: 234,
	graphType: "bar",
	values : [
        {
		    label: "to do",
		    code: "to-do",
		    color: "red",
		    value: 6
        },
        {
		    label: "in progress",
		    code: "in-progess",
		    color: "orange",
		    value: 10
        },
        {
		    label: "done",
		    code: "done",
		    color: "green",
		    value: 5,
        }
    ]
}

let GraphCardBodyText = 
{
    methodologyLabel: "Maecenas eget augue in augue dapibus",
    documentLabel: "Maecenas eget augue in augue  dapibus",
    documentId: 234,
    graphType: "text",
    values: [{
        label: "to do",
        code: "to-do",
        color: "red",
        value: 10
    }, {
        label: "in progress",
        code: "in-progess",
        color: "orange",
        value: 2
    }, {
        label: "done",
        code: "done",
        color: "green",
        value: 11,
    }, ]
}

let GraphCardBodyDonut = 
{
	methodologyLabel: "Lorem Ipsum",
	documentLabel: "Lorem ipsum dolor sit amet",
	documentId: 234,
	graphType: "donut",
	values: [
        {
		    label: "to do",
		    code: "to-do",
		    color: "red",
		    value: 6
        },
        {
		    label: "in progress",
		    code: "in-progess",
		    color: "orange",
		    value: 10
        },
        {
		    label: "done",
		    code: "done",
		    color: "green",
		    value: 5,
        }
    ]
}